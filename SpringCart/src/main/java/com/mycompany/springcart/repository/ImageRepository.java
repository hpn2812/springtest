/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.repository;

import com.mycompany.springcart.entities.ImageEntity;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

/**
 *
 * @author huynh
 */
@Repository
public interface ImageRepository extends CrudRepository<ImageEntity, Integer>{
    List<ImageEntity> findByNameLike(String imageName);
}
