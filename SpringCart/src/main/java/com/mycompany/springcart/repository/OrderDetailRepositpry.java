/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.repository;

import com.mycompany.springcart.entities.OrderDetailEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author huynh
 */
@Repository
public interface OrderDetailRepositpry extends CrudRepository<OrderDetailEntity, Integer>{
    
}
