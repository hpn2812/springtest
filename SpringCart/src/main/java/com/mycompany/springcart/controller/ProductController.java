/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.controller;

import com.mycompany.springcart.entities.ImageEntity;
import com.mycompany.springcart.entities.ProductEntity;
import com.mycompany.springcart.service.ImageService;
import com.mycompany.springcart.service.ProductService;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author huynh
 */
@Controller
public class ProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private ImageService imageService;
    
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }
    
    //Show bảng dữ liệu
     @RequestMapping(value = {"/", "home"}, method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("products", productService.getProducts());
        return "home";
    }
    //mapping page jsp product
    @RequestMapping("/add-product")
     public String addProduct(Model model) {
        model.addAttribute("product", new ProductEntity());
        model.addAttribute("action", "add");
        return "product";
    }
      @RequestMapping(value = "/result", method = RequestMethod.POST)
    public String resultProduct(
            Model model,
            @Valid @ModelAttribute("product") ProductEntity productEntity,
            @RequestParam("action") String action,
            @RequestParam("files") MultipartFile[] files,
            BindingResult result,
            HttpServletRequest request) {
        if (result.hasErrors()) {
            model.addAttribute("product", productService.getProductById(productEntity.getId()));
            model.addAttribute("action", action);
            return "/product";
        } else {
            //save table with relative one first
            productService.save(productEntity);

            for (MultipartFile file : files) {

                //then save table with relative many ones
                if (!file.isEmpty()) {
                    ImageEntity imageEntity = new ImageEntity();
                    //Create custom filename 
                    String fileNameTest = StringUtils.cleanPath(file.getOriginalFilename());
                    String newFileName = "";

                    //check image name exsits or not           
                    if (!imageService.getListImage().contains(fileNameTest)) {
                        newFileName = fileNameTest;
                    } else {
                        boolean cont = true;
                        do {
                            newFileName = RandomStringUtils.randomAlphabetic(4) + '_' + fileNameTest;
                            if (!imageService.getListImage().contains(newFileName)) {
                                cont = false;
                            }
                        } while (cont);
                    }

                    imageEntity.setName(newFileName);
                    imageEntity.setProductEntity(productEntity);
                    imageService.saveImage(imageEntity);

                    //save file to server
                    String saveFilePath = "C:\\Users\\huynh\\OneDrive\\Documents\\NetBeansProjects\\SpringCart\\src\\main\\webapp\\resources\\images";
                    System.out.println(saveFilePath + " " + newFileName);
                    try {
                        byte barr[] = file.getBytes();
                        BufferedOutputStream bout = new BufferedOutputStream(
                                new FileOutputStream(saveFilePath + File.separator + newFileName));
                        bout.write(barr);
                        bout.flush();
                        bout.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
                
        }
        return "redirect:/home";
    }
    
      @RequestMapping("/delete-product/{id}")
    public String deleteBook(Model model,
            @PathVariable("id") int id) {
        if (!productService.deleteProduct(id)) {
            return "redirect:/home?"
                    + "mesType=success&mes=Delete book id: " + id + " success!!!";
        } else {
            return "redirect:/home?"
                    + "mesType=error&mes=Delete_book_id: " + id + "_fail!!!";
        }
    }
     
     
}
