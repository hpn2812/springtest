/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.entities;


import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author huynh
 */
@Component
@Scope("session")
public class Cart {
    
    private OrderEntity orders;

    public Cart() {
        orders= new OrderEntity();
    }
    
    

    public Cart(OrderEntity orders) {
        this.orders = orders;
    }

    public OrderEntity getOrders() {
        return orders;
    }

    public void setOrders(OrderEntity orders) {
        this.orders = orders;
    }
    
    
    
}
