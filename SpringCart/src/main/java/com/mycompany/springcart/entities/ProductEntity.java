/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author huynh
 */
@Entity
@Table(name="product")
public class ProductEntity implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
     private int id;
    
    @Column(length = 50,nullable = false)
    @NotBlank
    private String name;
    
    @Column(nullable = false)
    private double price;
    
    @Column(name = "publish_date")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date publishDate;
    
    @ManyToOne
    @JoinColumn(name = "category_id")
    private CategoryEntity categoryEntities;
    
    @OneToMany(mappedBy = "productEntity",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Set<ImageEntity> imageEntities;
    
    @OneToMany(mappedBy = "productEntity",fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private  Set<OrderDetailEntity> detailEntities;

    public ProductEntity() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public CategoryEntity getCategoryEntities() {
        return categoryEntities;
    }

    public void setCategoryEntities(CategoryEntity categoryEntities) {
        this.categoryEntities = categoryEntities;
    }

   

    public Set<ImageEntity> getImageEntities() {
        return imageEntities;
    }

    public void setImageEntities(Set<ImageEntity> imageEntities) {
        this.imageEntities = imageEntities;
    }

    public Set<OrderDetailEntity> getDetailEntities() {
        return detailEntities;
    }

    public void setDetailEntities(Set<OrderDetailEntity> detailEntities) {
        this.detailEntities = detailEntities;
    }



  
    
    
    
    
}
