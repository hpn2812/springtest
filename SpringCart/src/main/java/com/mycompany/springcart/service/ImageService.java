/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.springcart.service;

import com.mycompany.springcart.entities.ImageEntity;
import com.mycompany.springcart.repository.ImageRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author huynh
 */
@Service
public class ImageService {
    @Autowired
    private ImageRepository imageRepository;
    
    public List<ImageEntity> getListImage() {
        return (List<ImageEntity>) imageRepository.findAll();
    }
    
    public void saveImage(ImageEntity imageEntity){
        imageRepository.save(imageEntity);
    }
    
    public List<ImageEntity> getListImageName(String name){
        return imageRepository.findByNameLike(name);
    }
    
    public void deleteImage(int id) {
        imageRepository.deleteById(id);
    }
    
}
